<!-- Briefly describe the announcement. -->

## Before publishing

* [ ] Check for common spelling errors (`setlocal spell spelllang=en` in Vim)
* [ ] Make sure all links (if any) are working
* [ ] Preview locally to make sure the article is rendering properly

## After publishing

* [ ] Submit to <https://www.reddit.com/r/inko/>
* [ ] Tweet about it
* [ ] Post a link to the article in the Matrix channel
